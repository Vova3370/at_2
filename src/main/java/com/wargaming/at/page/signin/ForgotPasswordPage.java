package com.wargaming.at.page.signin;

import com.wargaming.at.enums.Msg;
import com.wargaming.at.enums.NamePage;
import com.wargaming.at.model.User;
import com.wargaming.at.page.APage;
import com.wargaming.at.page.Loadable;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;
import static com.wargaming.at.model.Message.isExpectedMsgOf;

/**
 * страница ForgotPassword
 */

public class ForgotPasswordPage extends APage implements Loadable {

    @FindBy(xpath = "//span[contains(text(),'Retrieve Password')]")
    WebElement submit;

    @FindBy(css = "div[id='columns'] > div:first-child")
    WebElement forgotPage;

    public ForgotPasswordPage() {
        checkTransitionToNextPageOnText(NamePage.FORGOT_PASSWORD,forgotPage);
    }

    /**
     * Метод выполняет нажатие на ForgotPassword
     */

    public ForgotPasswordPage clickRetrievePassword() {
        expectedConditionVisibilityOf(submit);
        submit.click();
        return this;
    }

    /**
     * Метод выполняет функцию проверки сообщения при вводе невалидного значения
     */

    public ForgotPasswordPage asserMsgForgotPassword(User user) {
        WebElement centerColumn = getDriver().findElement(By.cssSelector("div[id='center_column'] div > p"));
        expectedConditionVisibilityOf(centerColumn);
        Assert.assertTrue(isExpectedMsgOf(Msg.SEND_YOUR_ADDRESS, centerColumn, user));
        return this;
    }

}
