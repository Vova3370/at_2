package com.wargaming.at.page.signin;

import com.wargaming.at.enums.NamePage;
import com.wargaming.at.page.APage;
import com.wargaming.at.page.Loadable;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.math.BigDecimal;

import static com.wargaming.at.dao.util.ExpectedCondition.clickWhenReady;

/**
 * Страница создание клиента
 */

public class CreateAccountPage extends APage implements Loadable {

    private static final Logger LOG = Logger.getLogger(CreateAccountPage.class);

    private final String loginStatic;
    private final String passwordStatic;

    @FindBy(id = "email_create")
    WebElement emailCreate;

    @FindBy(id = "submitAccount")
    WebElement submitAccount;

    @FindBy(id = "SubmitCreate")
    WebElement SubmitCreate;

    @FindBy(id = "id_gender1")
    WebElement id_gender1;

    @FindBy(id = "customer_firstname")
    WebElement customer_firstname;

    @FindBy(id = "customer_lastname")
    WebElement customer_lastname;

    @FindBy(id = "passwd")
    WebElement passwd;

    @FindBy(id = "address1")
    WebElement address1;

    @FindBy(id = "city")
    WebElement cityId;

    @FindBy(id = "id_state")
    WebElement uniform_state;

    @FindBy(id = "postcode")
    WebElement postcode;

    @FindBy(id = "other")
    WebElement other;

    @FindBy(id = "phone")
    WebElement phone;

    public String getLoginStatic() {
        return loginStatic;
    }

    public String getPasswordStatic() {
        return passwordStatic;
    }

    public CreateAccountPage() {
        checkTransitionToNextPage(NamePage.AUTHENTICATION_PAGE);
        loginStatic = "TestLogin" + RandomStringUtils.randomNumeric(5) + "@mail.ru";
        passwordStatic ="TestPassword" + RandomStringUtils.randomNumeric(5);
    }

    /**
     * Выбирает создать пол
     *
     * @param = String loginStatic
     */

    private void fillChooseGender() {
        LOG.info("Выбрать пол");
        id_gender1.click();
    }

    /**
     * Метод выполняет функцию нажать на кнопку создать клиента
     */

    private void writeAndClick() {
        LOG.info("Создать новый аккаунт " + loginStatic);
        emailCreate.sendKeys(loginStatic);
        LOG.info("Нажать кнопку - 'Создать клиента'");
        SubmitCreate.click();
    }

    /**
     * Метод выполняет функции по заполнению пароля
     */

    private void fillPassword() {
        LOG.info("Заполнить пароль" + passwordStatic);
        passwd.sendKeys(passwordStatic);
    }

    /**
     * Метод выполняет функции по заполнению Имени
     */

    private void fillName(String firstName) {
        LOG.info("Заполнить имя " + customer_firstname.getText());
        customer_firstname.sendKeys(firstName);
    }

    /**
     * Метод выполняет функции по заполнению фамилии
     */
    private void fillLastName(String lastName) {
        LOG.info("Заполнить фамилию " + customer_lastname.getText());
        customer_lastname.sendKeys(lastName);
    }


    /**
     * Метод выполняет функции по заполнению Адреса
     */
    private void fillAddress(String address) {
        LOG.info("Заполнить адрес" + address);
        address1.sendKeys(address);
        LOG.info("Успешно.");
    }

    /**
     * Метод выполняет функции по заполнению города
     */

    private void fillCity(String city) {
        LOG.info("Заполнить город" + city);
        cityId.sendKeys(city);
        LOG.info("Успешно.");
    }

    /**
     * Метод выполняет функции по заполнению Госудасртво
     */


    private void fillState(String state) {
        LOG.info("Заполнить государство" + state);
        uniform_state.click();
        Select select = new Select(uniform_state);
        select.selectByVisibleText(state);
        LOG.info("Успешно.");
    }

    /**
     * Метод выполняет функции по заполнению код
     */


    private void fillOtherCode(BigDecimal otherCode) {
        LOG.info("Заполнить код " + otherCode);
        postcode.sendKeys(otherCode.toString());
        LOG.info("Успешно.");
    }

    /**
     * Метод выполняет функции по заполнению телефона
     */


    private void fillPhone(BigDecimal phoneMob) {
        LOG.info("Заполнить телефон " + phoneMob);
        phone.sendKeys(phoneMob.toString());
        LOG.info("Успешно.");
    }

    /**
     * Метод нажимает на кнопку регестрации
     */


    private void clickSubmitAccount() {
        LOG.info("Нажать на кнопку регистрации");
        clickWhenReady(submitAccount);
        LOG.info("Успешно.");
    }

    /**
     * Метод выполняет функции по заполнению поля Иное
     */

    private void fillOther(String register) {
        LOG.info("Заполнить другое" + register);
        other.sendKeys(register);
        LOG.info("Успешно.");
    }

    /**
     * Метод создает нового клиента
     *
     * @param = String loginStatic
     */

    public CreateAccountPage createNewUnderhandAssertMail(String firstName, String lastName, String useState,
                                                          BigDecimal otherCode, String address, String city, String register, BigDecimal phoneMob) {
        writeAndClick();
        fillChooseGender();
        fillName(firstName);
        fillLastName(lastName);
        fillPassword();
        fillAddress(address);
        fillCity(city);
        fillState(useState);
        fillOtherCode(otherCode);
        fillOther(register);
        fillPhone(phoneMob);
        clickSubmitAccount();
        return this;
    }


}
