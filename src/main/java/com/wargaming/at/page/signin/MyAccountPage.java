package com.wargaming.at.page.signin;

import com.wargaming.at.enums.NamePage;
import com.wargaming.at.page.APage;
import com.wargaming.at.page.Loadable;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;
import static org.testng.Assert.assertTrue;

/**
 * страница моего аккаунта
 */

public class MyAccountPage extends APage implements Loadable {

    private static final Logger logger = Logger.getLogger(LoginPage.class);

    @FindBy(id = "center_column")
    WebElement centerColumn;

    @FindBy(xpath = "//span[contains(text(),'My personal information')]")
    WebElement personalInformation;

    public MyAccountPage() {
        checkTransitionToNextPage(NamePage.MY_ACCOUNT_PAGE);
    }


    public MyAccountPage assertMyAccount() {
        logger.info("Проверка перехода на страницу пользователя");
        expectedConditionVisibilityOf(centerColumn);
        assertTrue(getDriver().getTitle().contains("My account"));
        return this;
    }

    public MyAccountPage clickpersonalInformation() {
        LOG.info("Нажать на кнопку получение информации о моем профиле");
        personalInformation.click();
        return this;
    }

}
