package com.wargaming.at.page.signin;

import com.wargaming.at.enums.Msg;
import com.wargaming.at.enums.NamePage;
import com.wargaming.at.model.User;
import com.wargaming.at.page.APage;
import com.wargaming.at.page.Loadable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.apache.log4j.Logger;
import org.testng.Assert;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.util.ExpectedCondition.clickWhenReady;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;
import static com.wargaming.at.model.Message.isExpectedMsgOf;

/**
 * страница Авторизации, работа с основными методами
 */

public class LoginPage extends APage implements Loadable {

    private static final Logger logger = Logger.getLogger(LoginPage.class);

    @FindBy(id = "email")
    WebElement email;

    @FindBy(id = "passwd")
    WebElement password;

    @FindBy(id = "SubmitLogin")
    WebElement SubmitLogin;

    @FindBy(id = "email_create")
    WebElement emailCreate;

    @FindBy(id = "SubmitCreate")
    WebElement SubmitCreate;

    @FindBy(css = "form[id='login_form'] a")
    WebElement lost_password;

    public LoginPage() {
        checkTransitionToNextPage(NamePage.AUTHENTICATION_PAGE);
    }

    /**
     * Метод авторизации пользователя
     */
    public LoginPage sign(User user) {
        logger.info("Авторизация пользователя" + user);
        email.sendKeys(user.getLogin());
        password.sendKeys(user.getPassword());
        clickSubmitLogin();
        logger.info("Успешно.");
        return this;
    }

    /**
     * Метод заполнения мейла
     */

    public LoginPage sendEmail(User user) {
        expectedConditionVisibilityOf(email);
        email.sendKeys(user.getLogin());
        return this;
    }

    /**
     * Нажать на кнопку авторизациия
     */
    public LoginPage clickSubmitLogin() {
        clickWhenReady(SubmitLogin);
        return this;
    }

    /**
     * Метод заполняет мейл и нажимает создать аккаунт
     */

    public LoginPage sendMailAndCreate(User user) {
        LOG.info("Создать новый аккаунт" + user.getLogin());
        emailCreate.sendKeys(user.getLogin());
        LOG.info("Нажать кнопку - 'Создать клиента'" + SubmitCreate.getText());
        SubmitCreate.click();
        return this;
    }

    /**
     * Метод проверки сообщения
     */

    public LoginPage assertMessagesLoginPage(Msg msg) {
        WebElement centerLogin = getDriver().findElement(By.xpath("//*[@id='create_account_error']/ol/li"));
        Assert.assertTrue(isExpectedMsgOf(msg, centerLogin));
        return this;
    }

    /**
     * Нажать на кнопку забыли пароль
     */

    public LoginPage clickForgotPassword() {
        expectedConditionVisibilityOf(lost_password);
        lost_password.click();
        return this;
    }

    public LoginPage sendFailAuthorization(User user) {
        email.sendKeys(user.getLogin());
        password.sendKeys(user.getPassword());
        clickSubmitLogin();
        return this;
    }


    public void sign(String emailST, String passwordST) {
        email.sendKeys(emailST);
        password.sendKeys(passwordST);
        clickSubmitLogin();
    }

}
