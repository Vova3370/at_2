package com.wargaming.at.page.signin;

import com.wargaming.at.enums.NamePage;
import com.wargaming.at.page.APage;
import com.wargaming.at.page.Loadable;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;


public class MyPersonalInformationPage extends APage implements Loadable {

    @FindBy(id = "email")
    WebElement emailID;

    @FindBy(id = "lastname")
    WebElement lastNameElement;

    @FindBy(id = "firstname")
    WebElement firstNameLocator;

    @FindBy(xpath = "//span[contains(text(),' Your personal information')]")
    WebElement textTitle;

    public MyPersonalInformationPage() {
        checkTransitionToNextPage(NamePage.YOUR_PERSONAL_INFORMATION, textTitle);
    }

    /**
     * Данный метод сранивает данные после создания пользователя в разделе YOUR PERSONAL INFORMATION
     */
    public MyPersonalInformationPage assertPersonalData(String firstName, String lastName, String email) {
        LOG.info("Сранивает данные после создания клиена");
        Assert.assertEquals(firstName, firstNameLocator.getAttribute("value"), "Сравнение имени");
        Assert.assertEquals(lastName, lastNameElement.getAttribute("value"), "Сравнение фамилии");
        Assert.assertEquals(email, emailID.getAttribute("value"), "Сравнение мейла");
        LOG.info("Успешно.");
        return this;
    }

}
