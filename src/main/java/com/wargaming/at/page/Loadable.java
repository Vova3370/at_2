package com.wargaming.at.page;

import com.wargaming.at.dao.util.ExpectedCondition;
import com.wargaming.at.enums.NamePage;
import com.wargaming.at.page.title.MainPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;

/**
 * Данный интерфейс используется для проверки перехода страницы
 */

public interface Loadable {

    Logger LOG = Logger.getLogger(Loadable.class);

    default MainPage checkTransitionToNextPage(NamePage namePage) {
        try {
            ExpectedCondition.expectedConditionWaitNewTextInElement(namePage.getPageText());
            LOG.info("Успешный переход на страницу: " + namePage.getPageText());
        } catch (NoSuchElementException | TimeoutException | StaleElementReferenceException e) {
            LOG.error("Ошибка." + e.getMessage());
        }
        return new MainPage();
    }

    default MainPage checkTransitionToNextPage(NamePage namePage, WebElement webElement) {
        try {
            ExpectedCondition.expectedConditionWaitNewTextInElement(namePage.getPageText(),webElement);
            LOG.info("Успешный переход на страницу: " + namePage.getPageText());
        } catch (NoSuchElementException | TimeoutException | StaleElementReferenceException e) {
            LOG.error("Ошибка." + e.getMessage());
        }
        return new MainPage();
    }

    default MainPage checkTransitionToNextPageOnText(NamePage namePage, WebElement webElement) {
        try {
            new WebDriverWait(getDriver(), 10)
                    .until(ExpectedConditions.textToBePresentInElement(webElement, namePage.getPageText()));
            LOG.info("Успешный переход на страницу: " + namePage.getPageText());
        } catch (NoSuchElementException | TimeoutException | StaleElementReferenceException e) {
            LOG.error("Ошибка." + e.getMessage());
        }
        return new MainPage();
    }
}
