package com.wargaming.at.page.title;


import com.wargaming.at.page.APage;
import com.wargaming.at.page.Loadable;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.apache.log4j.Logger;

import static com.wargaming.at.dao.util.ExpectedCondition.clickWhenReady;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;

/**
 * Данный класс хранит в себе работу с главной страницой
 */

public class MainPage extends APage implements Loadable {

    private static final Logger logger = Logger.getLogger(MainPage.class);

    @FindBy(xpath = "//*[contains(text(), 'Sign in')]")
    public WebElement login;

    @FindBy(id = "search_query_top")
    public WebElement searhQueryTop;

    @FindBy(name = "submit_search")
    public WebElement searchButton;

    @FindBy(className = "logout")
    public WebElement logout;

    @FindBy(css = "div[id='columns'] span[class$='_page']")
    public WebElement page;

    @FindBy(id = "contact-link")
    WebElement contactLink;

    public MainPage() {
    }

    /**
     * Нажать на кнопку Sign in
     */

    public MainPage open() {
        expectedConditionVisibilityOf("header_logo");
        login.click();
        logger.info("Нажатие на кнопку Sign in");
        return this;
    }

    /**
     * Нажать на кнопку поиск товара
     */

    public MainPage searchProduct(String searchText) {
        searhQueryTop.clear();
        searhQueryTop.sendKeys(searchText);
        clickWhenReady(searchButton);
        logger.info("Нажатие на кнопку Поиск товара");
        return this;

    }

    /**
     * Нажать на кнопку Выйти из своего аккаунта
     */

    public MainPage logOutAccount() {
        logger.info("Нажать на кнопку -'Выйти из своего аккаунта'");
        logout.click();
        return this;
    }

    /**
     * Нажать на кнопку Contact us
     */
    public MainPage clickContcatLink() {
        logger.info("Нажать на кнопку -'Contact us'");
        contactLink.click();
        return this;
    }
}
