package com.wargaming.at.page.title;


import com.wargaming.at.enums.Msg;
import com.wargaming.at.enums.NamePage;
import com.wargaming.at.page.APage;
import com.wargaming.at.page.Loadable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.model.Message.isExpectedMsgOf;


/**
 * Страница поиска клиента
 */

public class SearchPage extends APage implements Loadable {

    @FindBy(css = "[id='center_column'] h1 span[class$='counter']")
    public WebElement center_column;

    @FindBy(css = "[id='center_column'] li >div")
    public List<WebElement> product_container;

    public SearchPage() {
        checkTransitionToNextPage(NamePage.SEARCH);
    }

    /**
     * Проверка найденного товара
     */
    public SearchPage assertSearchProduct(String searchTextData) {
        LOG.info("Проверка отображение найденого товара");
        WebElement centerProduct = getDriver().findElement(By.cssSelector("div[id='center_column'] div > h5 > a"));
        Assert.assertEquals(centerProduct.getText(), searchTextData);
        LOG.info("Успешно.");
        return this;
    }

    public SearchPage assertMessagesProduct(Msg msg) {
        WebElement centerProduct = getDriver().findElement(By.xpath("//*[@id='center_column']/p"));
        Assert.assertTrue(isExpectedMsgOf(msg, centerProduct));
        return this;
    }

    /**
     * Данный метод служит для сравнения даных найденного товара
     */

    public SearchPage getTextBeenFound() {
        center_column.getText();
        LOG.info("Получение элемента" + center_column.getText());
        ArrayList<String> list = new ArrayList<>();
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(center_column.getText());
        while (m.find()) {
            list.add(m.group());
        }
        Assert.assertEquals(product_container.size(), Integer.parseInt(list.stream().reduce("", (a, b) -> a + b)));
        return this;
    }

}
