package com.wargaming.at.page;

import org.openqa.selenium.support.PageFactory;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;

public class APage {

    public APage() {
        PageFactory.initElements(getDriver(), this);
    }
}
