package com.wargaming.at.page.contact_us;

import com.wargaming.at.enums.Msg;
import com.wargaming.at.enums.NamePage;
import com.wargaming.at.enums.SubjectHeading;
import com.wargaming.at.model.User;
import com.wargaming.at.page.APage;
import com.wargaming.at.page.title.MainPage;
import com.wargaming.at.page.Loadable;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.apache.log4j.Logger;
import org.testng.Assert;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;
import static com.wargaming.at.model.Message.isExpectedMsgOf;

/**
 * Класс страницы ContactUsPage
 */
public class ContactUsPage extends APage implements Loadable {

    private static final Logger LOG = Logger.getLogger(MainPage.class);

    @FindBy(id = "contact-link")
    WebElement contactLink;

    @FindBy(id = "email")
    WebElement email;

    @FindBy(id = "message")
    WebElement message;

    @FindBy(id = "id_contact")
    WebElement idOcontact;

    @FindBy(id = "submitMessage")
    WebElement submitMessage;

    @FindBy(id = "contact")
    WebElement contact;

    public ContactUsPage() {
        checkTransitionToNextPage(NamePage.CONTACT_PAGE);
    }

    /**
     * Метод отправки сообщения
     */
    public ContactUsPage postingMessages(User user, String messages, SubjectHeading subjectHeading) {
        idOcontact.click();
        fillSubjectHeading(subjectHeading);
        expectedConditionVisibilityOf(email);
        fillMessages(user, messages);
        expectedConditionVisibilityOf(submitMessage);
        submitMessage.click();
        return this;
    }

    /**
     * Метод отправки сообщения
     */

    public ContactUsPage contactClick() {
        contact.click();
        return this;
    }

    /**
     * Нажать отправить сообщение
     */
    public ContactUsPage clickSubmitMessage() {
        contactLink.click();
        LOG.info("Нажать - Отправить сообщение");
        expectedConditionVisibilityOf(submitMessage);
        submitMessage.click();
        return this;
    }

    /**
     * Проверка сообщения
     */
    public ContactUsPage assertMessagesContactUsPage(Msg msg) {
        WebElement centerColumn = getDriver().findElement(By.xpath("//*[@id='center_column']/p"));
        Assert.assertTrue(isExpectedMsgOf(msg, centerColumn));
        return this;
    }

    /**
     * Заполняет поле выбора сервера для отправки сообщения
     */

    public ContactUsPage fillSubjectHeading(SubjectHeading subjectHeading) {
        Select select = new Select(idOcontact);
        select.selectByVisibleText(subjectHeading.getSubjectText());
        return this;
    }

    /**
     * Заполняет поле сообщение
     */

    public ContactUsPage fillMessages(User user, String messages) {
        email.sendKeys(user.getLogin());
        expectedConditionVisibilityOf(message);
        message.sendKeys(messages);
        return this;
    }

}
