package com.wargaming.at.dao.util;

import com.wargaming.at.dao.driver.DriverCreator;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.annotation.Nullable;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;

/**
 * Утилитные методы
 */

public class ExpectedCondition {

    private final static Logger LOG = Logger.getLogger(DriverCreator.class);

    private static int expectation = 10;

    private ExpectedCondition(WebDriver driver) {
    }

    public static WebElement expectedConditionVisibilityOf(int time, WebElement element) {
        new WebDriverWait(getDriver(), time).until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public static WebElement expectedConditionVisibilityOf(WebElement element) {
        new WebDriverWait(getDriver(), expectation).until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public static WebElement expectedConditionVisibilityOf(String s) {
        return new WebDriverWait(getDriver(), expectation).until(ExpectedConditions.visibilityOf(getDriver().findElement(By.id(s))));
    }


    public static void clickWhenReady(WebElement element, int timeout) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeout);
        WebElement el = wait.until(ExpectedConditions.elementToBeClickable(element));
        el.click();
    }

    public static void clickWhenReady(WebElement element) {
        WebDriverWait wait = new WebDriverWait(getDriver(), expectation);
        WebElement el = wait.until(ExpectedConditions.elementToBeClickable(element));
        el.click();
    }

    public static void scrollToViewElement(WebElement element) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static void expectedConditionWaitNewTextInElement(String expected) {
        WebElement element = getDriver().findElement(By.cssSelector("div[id='columns'] > div > span:last-child"));
        waitNewText(element, expected, expectation);
    }

    public static void expectedConditionWaitNewTextInElement(String expected,WebElement element) {
        element.getText();
        waitNewText(element, expected, expectation);
    }


    private static void waitNewText(WebElement element, String expected, int expectation) {
        new WebDriverWait(getDriver(), expectation).until(
                new org.openqa.selenium.support.ui.ExpectedCondition<Boolean>() {
                    @Nullable
                    @Override
                    public Boolean apply(@Nullable WebDriver input) {
                        if (expected.equals(element.getText())) {
                            LOG.info("Заголовок изменен: " + element.getText());
                            return true;
                        }
                        LOG.info("Заголовок: " + element.getText());
                        return false;
                    }
                }
        );
    }
}
