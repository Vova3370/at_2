package com.wargaming.at.enums;


/**
 * Данный енам хранит наименование страниц
 */

public enum NamePage {

    AUTHENTICATION_PAGE("Authentication"),
    MY_ACCOUNT_PAGE("My account"),
    CONTACT_PAGE("Contact"),
    FORGOT_PASSWORD("Forgot your password"),
    YOUR_PERSONAL_INFORMATION("Your personal information"),
    SEARCH("Search");


    private String pageText;

    NamePage(String pageText) {
        this.pageText = pageText;
    }

    public String getPageText() {
        return pageText;
    }

    public void setPageText(String pageText) {
        this.pageText = pageText;
    }

}
