package com.wargaming.at.enums;


/**
 * Данный енам хранит сообщения в БП
 */

public enum Msg {

    EMAIL_REQUIRED(MsgType.ERROR, "An email address required."),
    INVALID_EMAIL_ADDRESS(MsgType.ERROR, "Invalid email address."),
    PASSWORD_REQUIRED(MsgType.ERROR, "Password is required."),
    AUTHENTICATION_FAIL(MsgType.ERROR, "Authentication failed."),
    MESSAGE_SUCCESSFULLY(MsgType.INFO, "Your message has been successfully sent to our team."),
    ACCOUNT_USING_THIS_EMAIL(MsgType.ERROR, "An account using this email address has already been registered. Please enter a valid password or request a new one."),
    SEARCH_KEYWORD(MsgType.INFO, "Please enter a search keyword"),
    SEND_YOUR_ADDRESS(MsgType.INFO, "A confirmation email has been sent to your address: ");

    private MsgType msgType;
    private String msgText;

    Msg(MsgType msgType, String msgText) {
        this.msgType = msgType;
        this.msgText = msgText;
    }

    public String getMsgText() {
        return msgText;
    }

    public MsgType getMsgType() {
        return msgType;
    }

}

