package com.wargaming.at.enums;

/**
 * Данный енам хранит наименование серверов
 */

public enum SubjectHeading {

    CUSTOMER_SERVICE("Customer service"),
    WEBMASTER("Webmaster");

    SubjectHeading(String subjectText) {
        this.subjectText = subjectText;
    }

    private String subjectText;

    public String getSubjectText() {
        return subjectText;
    }

    public void setSubjectText(String subjectText) {
        this.subjectText = subjectText;
    }


}
