package com.wargaming.at.enums;


/**
 * Данный енам хранит тип сообщений
 */

public enum MsgType {

    INFO("info"),
    ERROR("error");

    private String strType;

    MsgType(String strType) {
        this.strType = strType;
    }

    public String getStrType() {
        return strType;
    }

}
