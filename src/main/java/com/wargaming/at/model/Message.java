package com.wargaming.at.model;

import com.wargaming.at.enums.Msg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.apache.log4j.Logger;

import java.io.Serializable;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;

/**
 * Данный класс хранит себя работу с Сообщениями
 */

public class Message implements Serializable {

    private static final Logger LOG = Logger.getLogger(Message.class);

    public final static String MESSAGE_COMPARE_TEXT = "Результат сравнения сообщений с ожидаемым: ";

    public Message() {
    }

    /**
     * Сравнивает ожидаемое сообщение с полученным.
     *
     * @return
     */
    public static boolean isExpectedMsgOf(Msg msg) {
        WebElement centerColumn = getDriver().findElement(By.cssSelector("div[id='center_column'] > div li"));
        expectedConditionVisibilityOf(centerColumn);
        centerColumn.getText();
        LOG.info("Ожидается сообщение: " + msg.getMsgText());
        if (msg.getMsgText().equalsIgnoreCase(centerColumn.getText())) {
            LOG.info(MESSAGE_COMPARE_TEXT + true);
            return true;
        }
        return false;
    }

    public static boolean isExpectedMsgOf(Msg msg,WebElement element) {
        expectedConditionVisibilityOf(element);
        element.getText();
        LOG.info("Ожидается сообщение: " + msg.getMsgText());
        if (msg.getMsgText().equalsIgnoreCase(element.getText())) {
            LOG.info(MESSAGE_COMPARE_TEXT + true);
            return true;
        }
        return false;
    }

    public static boolean isExpectedMsgOf(Msg msg,WebElement element,User user) {
        String element9 = msg.getMsgText() + user.getLogin();
        expectedConditionVisibilityOf(element);
        element.getText();
        LOG.info("Ожидается сообщение: " +element9);
        if (element9.equalsIgnoreCase(element.getText())) {
            LOG.info(MESSAGE_COMPARE_TEXT + true);
            return true;
        }
        return false;
    }

    /**
     * Проверяет есть ли сообщение на странице без логирования.
     *
     * @return
     */
    public static boolean isVisibilityMsgQuiet() {
        WebElement element = getDriver().findElement(By.xpath("div[id='center_column'] > div li"));
        element.getText();
        if (element != null) {
            return true;
        }
        return false;
    }

}
