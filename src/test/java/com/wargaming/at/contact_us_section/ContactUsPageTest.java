package com.wargaming.at.contact_us_section;

import com.wargaming.at.ASuite.TestSuiteSetup;
import com.wargaming.at.enums.SubjectHeading;
import com.wargaming.at.model.User;
import com.wargaming.at.page.contact_us.ContactUsPage;
import com.wargaming.at.page.title.MainPage;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.wargaming.at.enums.Msg.INVALID_EMAIL_ADDRESS;
import static com.wargaming.at.enums.Msg.MESSAGE_SUCCESSFULLY;
import static com.wargaming.at.model.Message.isExpectedMsgOf;

/**
 * Тесты для проверки отправки сообщений страницы ConstactUs
 */
public class ContactUsPageTest extends TestSuiteSetup {


    private static final Logger LOG = Logger.getLogger(ContactUsPageTest.class);

    @Test(groups = {"acceptance", "smoke"}, dataProvider = "getContractData")
    public void sendMessagesContract(User user, String message, SubjectHeading subjectHeading) {
        LOG.info("Тест отправки сообщение на странице contactLink с проверкой сообщения(позитивный ценарий)");
        new MainPage()
                .clickContcatLink();
        new ContactUsPage()
                .postingMessages(user, message, subjectHeading)
                .assertMessagesContactUsPage(MESSAGE_SUCCESSFULLY);
        new ContactUsPage()
                .contactClick();
    }

    @Test(groups = {"acceptance"})
    public void sendSubmitMessage() {
        LOG.info("Тест отправки сообщение с не заполнеными полями на странице contactLink с проверкой сообщения об ошибке");
        new ContactUsPage()
                .clickSubmitMessage();
        Assert.assertTrue(isExpectedMsgOf(INVALID_EMAIL_ADDRESS));
    }


    @DataProvider(name = "getContractData")
    public Object[][] contractData() {
        return new Object[][]{
                {new User("dozer_8@mail.ru"), "Test test test", SubjectHeading.CUSTOMER_SERVICE},
                {new User("dozer_8@mail.ru"), "Test2 test2 test2", SubjectHeading.WEBMASTER}
        };
    }
}
