package com.wargaming.at.product_title_section;

import com.wargaming.at.ASuite.TestSuiteSetup;
import com.wargaming.at.page.title.MainPage;
import com.wargaming.at.page.title.SearchPage;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.wargaming.at.enums.Msg.SEARCH_KEYWORD;

/**
 * Тесты для проверки поиска товара
 */

public class ProductSearchTest extends TestSuiteSetup {

    private static final Logger LOG = Logger.getLogger(ProductSearchTest.class);

    @Test(groups = {"smoke", "acceptance"}, dataProvider = "getDataOneProduct")
    public void searchProductTest(String textProduct) {
        LOG.info("Тест поиск одного товара с корретным отображением найденого товара ");
        new MainPage()
                .searchProduct(textProduct);
        new SearchPage()
                .assertSearchProduct(textProduct);
    }

    @Test(groups = "acceptance", dataProvider = "getProduct")
    public void searchOneProductTest(String oneProduct) {
        LOG.info("Тест поиск несколько товаров с проверкой найденного товара ");
        new MainPage()
                .searchProduct(oneProduct);
        new SearchPage()
                .getTextBeenFound();
    }

    @Test(groups = "acceptance", dataProvider = "getDataNull")
    public void searchAssertMsgTest(String dataNull) {
        LOG.info("Тест поиск товара с нулевыми входными даннми и проверкой сообщения");
        new MainPage()
                .searchProduct(dataNull);
        new SearchPage()
                .assertMessagesProduct(SEARCH_KEYWORD);
    }

    @DataProvider(name = "getDataOneProduct")
    public static Object[] getUser() {
        return new String[]
                {"Faded Short Sleeve T-shirts"};
    }

    @DataProvider(name = "getProduct")
    public static Object[] getDataOneProduct() {
        return new String[]
                {"Dress"};
    }

    @DataProvider(name = "getDataNull")
    public static Object[] getDataNull() {
        return new String[]
                {""};
    }

}
