package com.wargaming.at.sign_in_section;

import com.wargaming.at.ASuite.TestSuiteSetup;
import com.wargaming.at.model.User;
import com.wargaming.at.page.title.MainPage;
import com.wargaming.at.page.signin.ForgotPasswordPage;
import com.wargaming.at.page.signin.LoginPage;
import com.wargaming.at.page.signin.MyAccountPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.apache.log4j.Logger;

import static com.wargaming.at.enums.Msg.*;
import static com.wargaming.at.model.Message.*;

/**
 * Тесты для проверки логина
 */

public class LoginTest extends TestSuiteSetup {

    private static final Logger LOG = Logger.getLogger(LoginTest.class);

    @Test(groups = {"smoke", "acceptance"}, dataProvider = "getUserLogin")
    public void forgotPasswordTest(User user) {
        LOG.info("Тест forgot страницы с проверкой сообщения");
        new MainPage()
                .open();
        new LoginPage()
                .clickForgotPassword()
                .sendEmail(user);
        new ForgotPasswordPage()
                .clickRetrievePassword()
                .asserMsgForgotPassword(user);

    }

    @Test(groups = {"smoke", "acceptance"})
    public void forgotPasswordAndAssertMessagesTest() {
        LOG.info("Тест Авторизации пользователя (Если забыли пароль)");
        new MainPage()
                .open();
        new LoginPage()
                .clickForgotPassword();
        new ForgotPasswordPage()
                .clickRetrievePassword();
        Assert.assertTrue(isExpectedMsgOf(INVALID_EMAIL_ADDRESS));
    }


    @Test(groups = {"smoke", "acceptance"}, dataProvider = "getUser")
    public void loginTest(User user) {
        LOG.info("Тест Авторизации пользователя (позитивный сценарий)");
        new MainPage()
                .open();
        new LoginPage()
                .sign(user);
        new MyAccountPage()
                .assertMyAccount();
        new MainPage()
                .logOutAccount();
    }

    @Test(groups = "acceptance")
    public void assertLoginEmailMessagesTest() {
        LOG.info("Тест авторизации пользователя с пустными входными данными");
        new MainPage()
                .open();
        new LoginPage()
                .clickSubmitLogin();
        Assert.assertTrue(isExpectedMsgOf(EMAIL_REQUIRED));
    }

    @Test(groups = "acceptance")
    public void assertLoginPasswordMessagesTest() {
        LOG.info("Тест авторизации пользователя с пустым паролем");
        new MainPage()
                .open();
        new LoginPage()
                .sendEmail(new User("dozer_8@mail.ru", ""))
                .clickSubmitLogin();
        Assert.assertTrue(isExpectedMsgOf(PASSWORD_REQUIRED));
    }

    @Test(groups = "acceptance")
    public void assertLoginAuthenticationFailedMessagesTest() {
        LOG.info("Тест авторизации пользователя с введенным не верным паролем");
        new MainPage()
                .open();
        new LoginPage()
                .sendFailAuthorization(new User("dozer_8@mail.ru", "testFAIL"))
                .clickSubmitLogin();
        Assert.assertTrue(isExpectedMsgOf(AUTHENTICATION_FAIL));
    }

    @DataProvider(name = "getUser")
    public static Object[] getUser() {
        return new User[]
                {new User("dozer_8@mail.ru", "vovakorzun")};
    }


    @DataProvider(name = "getUserLogin")
    public static Object[] getUserLogin() {
        return new User[]
                {new User("dozer_8@mail.ru")};

    }
}



