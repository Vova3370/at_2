package com.wargaming.at.sign_in_section;

import com.wargaming.at.ASuite.TestSuiteSetup;
import com.wargaming.at.model.User;
import com.wargaming.at.page.signin.MyAccountPage;
import com.wargaming.at.page.signin.MyPersonalInformationPage;
import com.wargaming.at.page.title.MainPage;
import com.wargaming.at.page.signin.CreateAccountPage;
import com.wargaming.at.page.signin.LoginPage;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static com.wargaming.at.enums.Msg.ACCOUNT_USING_THIS_EMAIL;

/**
 * Тесты для проверки создание клиента
 */

public class CreateUserTest extends TestSuiteSetup {

    private static final Logger LOG = Logger.getLogger(CreateUserTest.class);

    @Test(groups = {"acceptance", "smoke"}, dataProvider = "getUserData")
    public void createAccountAndAssert(String firstName, String lastName,String state,BigDecimal otherCode,
                                       String address, String city, String register, BigDecimal phone) {
        LOG.info("Создание нового клиента + авторизация новым пользователям с проверкой,что авторизовались корретным пользователям");
        new MainPage()
                .open();
        CreateAccountPage myAccountPage = new CreateAccountPage();
                myAccountPage.createNewUnderhandAssertMail(firstName, lastName, state, otherCode, address, city, register, phone);
        new MyAccountPage()
                .clickpersonalInformation();
        new MyPersonalInformationPage()
                .assertPersonalData(firstName,lastName,myAccountPage.getLoginStatic());
        new MainPage()
                .logOutAccount()
                .open();
        new LoginPage()
                .sign(myAccountPage.getLoginStatic(), myAccountPage.getPasswordStatic());
        new MainPage()
                .logOutAccount();

    }

    @Test(groups = {"acceptance"})
    public void createAccountAndAssertMessages() {
        LOG.info("Авторизация уже зарегестрированным пользователем");
        new MainPage()
                .open();
        new LoginPage()
                .sendMailAndCreate(new User("dozer_8@mail.ru"))
                .assertMessagesLoginPage(ACCOUNT_USING_THIS_EMAIL);

    }

    @DataProvider(name = "getUserData")
    public Object[][] userData() {
        return new Object[][]{
                {"TestName", "TestLastName","Alabama", BigDecimal.valueOf(30297), "Street address, P.O. Box, Company name, etc.", "Minsk",
                        "You must register at least one phone number.", BigDecimal.valueOf(3240722)}};
    }

}
